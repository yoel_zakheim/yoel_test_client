
import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private http: HttpClient) { }
  ngOnInit() {
    return this.http.get('https://jsonplaceholder.typicode.com/users').subscribe(data => {
      this.data = data;
    });
  }

  title = 'yoel-test';
  columns = ['ID', 'NAME', 'JOB'];
  data: any = [];
  
  // data:any = [
  //   {id: '1', name: 'yoel', job:'ppp'},
  //   {id: '2', name: 'levi', job:'jjj'}
  // ]; // => test

  editRowId: any;
  EditRowId(id) {
    this.editRowId = id;
  }
  
  submitData(user) {
    this.http.post('https://jsonplaceholder.typicode.com/posts', user).subscribe(data => {
      console.log(data);
    });
  }
}
